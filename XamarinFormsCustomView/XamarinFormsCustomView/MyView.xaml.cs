﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinFormsCustomView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyView : ContentView
	{

        public static readonly BindableProperty PlaceHolderProperty = 
            BindableProperty.Create(
                propertyName:"PlaceHolder", 
                returnType: typeof(string), 
                declaringType: typeof(MyView), 
                defaultValue: null,
               defaultBindingMode: BindingMode.TwoWay);


        public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(MyView), null, BindingMode.TwoWay);
        public static readonly BindableProperty CommandProperty = BindableProperty.Create("Command", typeof(ICommand), typeof(MyView), null);

        public string PlaceHolder
        {
            get { return (string)this.GetValue(PlaceHolderProperty); }
            set { SetValue(PlaceHolderProperty, value); }
        }

        public string Text
        {
            get { return (string)this.GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)this.GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
    


		public MyView ()
		{
			InitializeComponent ();
            this.stkLayout.BindingContext = this;
		}


	}
}