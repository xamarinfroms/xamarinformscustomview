﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinFormsCustomView
{
    public class MainViewModel
    {
        public string PlaceHolder { get; set; } = "我是提示";
        public string Text { get; set; }
        public ICommand OnClick
        {
            get
            {
                if (this.onClick == null)
                    onClick = new Command(() =>
                      {
                          PageHelper.Alert(this.Text);
                      });
                return this.onClick;
            }
        }

        public ICommand onClick;
    }
}
